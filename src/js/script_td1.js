function adapterGalerie(nom) {
    for (let i = 1; i <= 6; i++) {
      const image = document.getElementById('fleur' + i);
      // Mettre à jour la source de l'image
      image.src = 'img/fleurs/' + nom + '/' + nom + i + '.jpg';
      // Mettre à jour l'attribut title de l'image
      image.title = nom;
      // Mettre à jour l'attribut alt de l'image
      image.alt = nom + i;
    };
    adapterTitre(nom);
  }
  
  function cacher(im) {
      im.classList.remove('visible');
      im.style.transition = "opacity 3s";
      im.classList.add('cachee');
    }
  
    function afficher(im) {
      im.classList.remove('cachee');
      im.style.transition = "opacity 3s";
      im.classList.add('visible');
    }
  
    function suivant(n) {
      const num = Number(n); 
      return (num % 6) + 1;
  }
  
  
  function changeBanniereV1() {
    const tab = document.getElementsByClassName('visible'); 
      const currentImg = tab[0]; 
      cacher(currentImg); 
      const currentId = Number(currentImg.id); 
      const nextId = (currentId % 6) + 1; 
      const nextImg = document.getElementById(nextId.toString());
      afficher(nextImg);
    
  }

  function changeBanniereV2() {
    const tab = document.getElementsByClassName('visible'); 
      const currentImg = tab[0]; 
      cacher(currentImg); 
      const currentId = Number(currentImg.id); 
      const nextId = (currentId % 6) + 1; 
      const nextImg = document.getElementById(nextId.toString());
      afficher(nextImg);
    
  }

  function detruitInfobulle() {
    const info = document.getElementById('bulle');
    document.body.removeChild(info);
  }

  function construitInfobulle() {
    const info = document.createElement('div');
    info.innerHTML = "<p>c'est moi la bulle !</p>";
    info.id = "bulle";
    info.style.position = "fixed";
    info.style.top = "100px";
    info.style.right = "150px";
    info.style.padding = "20px";
    info.style.backgroundColor = "darkblue";
    info.style.color = "white";
    document.body.appendChild(info);
  }
  
  let chb = setInterval(changeBanniereV2, 6000);
  
  function adapterTitre(nom) {
    const titreElement = document.getElementById('titre');
    titreElement.textContent = tabTitres[nom] || 'Galerie de fleurs';
  }
  
  function stopperDefilement(){
    clearInterval(chb);
  }

  function lancerDefilement(){
    chb = setInterval(changeBanniereV2, 6000);
  }
  
  const tabTitres = {
    'rose' : 'Galerie de roses',
    'hortensia': 'Galerie d\’hortensias',
    'fruitier': 'Galerie de fruitiers',
    'autre': 'Galerie de fleurs diverses'
  };

  let dernierBg = 0; 
  
  function changerParametres() {
      let nouveauBg;
      do {
          nouveauBg = Math.floor(Math.random() * 4) + 1; 
        } while (nouveauBg === dernierBg);
      dernierBg = nouveauBg; 
      document.body.style.backgroundImage = `url('img/background/bg-${nouveauBg}.jpg')`;
      document.body.style.backgroundSize = 'cover'; 
      document.body.style.backgroundRepeat = 'no-repeat';
  }
  